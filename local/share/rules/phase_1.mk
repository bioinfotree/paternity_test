### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# determine which shell the program must use
SHELL = /bin/bash

context prj/radtools

PATH_FAM ?= 

CPUS ?=

PARENT ?=

MISMATCHES ?=


parent1.fna: $(PATH_FAM)
	rm -f $@; \
	for FILE in $(PATH_FAM)/AN-P1_LG402_*.fq_1; do \
	fastq_to_fasta < $$FILE >> $@; \
	done;
	#fastq2tab < $$FILE | wc -l; \


parent2.fna: $(PATH_FAM)
	rm -f $@; \
	for FILE in $(PATH_FAM)/AN-P2_LG403_*.fq_1; do \
	fastq_to_fasta < $$FILE >> $@; \
	done;


off1.fna: $(PATH_FAM)
	rm -f $@; \
	for FILE in $(PATH_FAM)/AN-O1_LG384_1_ATTAG.fq_1; do \
	fastq_to_fasta < $$FILE >> $@; \
	done;


off2.fna: $(PATH_FAM)
	rm -f $@; \
	for FILE in $(PATH_FAM)/AN-O2_LG385_1_CGTAT.fq_1; do \
	fastq_to_fasta < $$FILE >> $@; \
	done;


off3.fna: $(PATH_FAM)
	rm -f $@; \
	for FILE in $(PATH_FAM)/AN-O3_LG386_1_GACTA.fq_1; do \
	fastq_to_fasta < $$FILE >> $@; \
	done;

off5.fna: $(PATH_FAM)
	rm -f $@; \
	for FILE in $(PATH_FAM)/AN-O5_LG388_1_ACGTA.fq_1; do \
	fastq_to_fasta < $$FILE >> $@; \
	done;


off6.fna: $(PATH_FAM)
	rm -f $@; \
	for FILE in $(PATH_FAM)/AN-O6_LG389_1_TACGT.fq_1; do \
	fastq_to_fasta < $$FILE >> $@; \
	done;


off7.fna: $(PATH_FAM)
	rm -f $@; \
	for FILE in $(PATH_FAM)/AN-O7_LG398_1_TCGAG.fq_1; do \
	fastq_to_fasta < $$FILE >> $@; \
	done;






# bowtie index of both naccarii parents
parents_index.1.ebwt: parent1.fna parent2.fna
	bowtie-build -f $<,$^2 $(basename $(basename $@))

# bowtie index of parents 1 for comparison of parent 1 Vs parent 2
parent%_index.2.ebwt: parent%.fna
	bowtie-build -f $< $(basename $(basename $@))


# bowtie indexes files are several files called: off1_index.?.ebwt, were ? is a number.# but this number is not progressive. For some files I have:
# parent1_index.1.ebwt
# parent1_index.2.ebwt
# parent1_index.3.ebwt
# parent1_index.4.ebwt
# For other I have:
# parent2_index.2.ebwt
# parent2_index.3.ebwt
# parent2_index.4.ebwt
# Or:
# off2_index.1.ebwt
# off2_index.3.ebwt
# off2_index.4.ebwt
# the numbering is not constant
# instead, 2 files are always produced with constant name:
# ?.rev.1.ebwt
# ?.rev.2.ebwt
# so I use ?.rev.1.ebwt as target
off%_index.rev.1.ebwt: off%.fna
	bowtie-build -f $< $(basename $(basename $@))




ifeq ($(PARENT), both)
%.tab: parents_index.1.ebwt
	bowtie -p $(CPUS) -k 1 --best -v $(MISMATCHES) $(basename $(basename $<)) $(addprefix $(PATH_FAM), $(addsuffix .fq_1, $(basename $@))) > $@
endif

ifeq ($(PARENT), 1)
%.tab: parent1_index.1.ebwt
	bowtie -p $(CPUS) -k 1 --best -v $(MISMATCHES) $(basename $(basename $<)) $(addprefix $(PATH_FAM), $(addsuffix .fq_1, $(basename $@))) > $@
endif

ifeq ($(PARENT), 2)
%.tab: parent2_index.2.ebwt
	bowtie -p $(CPUS) -k 1 --best -v $(MISMATCHES) $(basename $(basename $<)) $(addprefix $(PATH_FAM), $(addsuffix .fq_1, $(basename $@))) > $@
endif









ifeq ($(PARENT), off1)
%.tab: off1_index.rev.1.ebwt
	bowtie -p $(CPUS) -k 1 --best -v $(MISMATCHES) $(basename $(basename $(basename $<))) $(addprefix $(PATH_FAM), $(addsuffix .fq_1, $(basename $@))) > $@
endif

ifeq ($(PARENT), off2)
%.tab: off2_index.rev.1.ebwt
	bowtie -p $(CPUS) -k 1 --best -v $(MISMATCHES) $(basename $(basename $<)) $(addprefix $(PATH_FAM), $(addsuffix .fq_1, $(basename $@))) > $@
endif

ifeq ($(PARENT), off3)
%.tab: off3_index.rev.1.ebwt
	bowtie -p $(CPUS) -k 1 --best -v $(MISMATCHES) $(basename $(basename $<)) $(addprefix $(PATH_FAM), $(addsuffix .fq_1, $(basename $@))) > $@
endif


ifeq ($(PARENT), off5)
%.tab: off5_index.rev.1.ebwt
	bowtie -p $(CPUS) -k 1 --best -v $(MISMATCHES) $(basename $(basename $<)) $(addprefix $(PATH_FAM), $(addsuffix .fq_1, $(basename $@))) > $@
endif


ifeq ($(PARENT), off6)
%.tab: off6_index.rev.1.ebwt
	bowtie -p $(CPUS) -k 1 --best -v $(MISMATCHES) $(basename $(basename $<)) $(addprefix $(PATH_FAM), $(addsuffix .fq_1, $(basename $@))) > $@
endif


ifeq ($(PARENT), off7)
%.tab: off7_index.rev.1.ebwt
	bowtie -p $(CPUS) -k 1 --best -v $(MISMATCHES) $(basename $(basename $<)) $(addprefix $(PATH_FAM), $(addsuffix .fq_1, $(basename $@))) > $@
endif






%.stat: %.tab
	paste \
	<(cat $(addprefix $(PATH_FAM), $(addsuffix .fq_1, $(basename $<))) \
	| fastq2tab \
	| wc -l \
	| bawk '!/^[$$,\#+]/ { \
	{ print "total_reads", $$0; } \
	}') \
	<(wc -l $< \
	| bawk '!/^[$$,\#+]/ { \
	{ print "aligned_reads", $$0; } \
	}') > $@


summary.tab: $(OFFS)
	> $@; \
	for FILE in *.stat; do \
	cat $$FILE >> $@; \
	done;





classpathify = $(subst $(eval),;,$(wildcard $1))
PHONY: test
test:
	@echo $(call classpathify,$(PATH_OFF))
	@echo $(subst $(space),$(comma),$(PATH_OFF))






# Standard Phony Targets for Users.

# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += $(OFFS) \
	 summary.tab


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += $(OFFS) \
	 summary.tab


# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	$(RM) *.stat


######################################################################
### phase_1.mk ends here
